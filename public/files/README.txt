Example files for importing:

- test requirements (requirements.xlsx)
- test cases with steps (testcases.xlsx)
- test results (testresults.xlsx)

To import test requirements you first need
to create requirement specifications (folders).
See create-req-spec.xml file or create them manually.

To import test results, the test cases must be assigned
to the test plans in your TestLink.