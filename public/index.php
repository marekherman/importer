<?php
//error_reporting(-1);
//ini_set('display_errors', 'On');

if (!defined('APPLICATION_PATH')) {
    define('APPLICATION_PATH', realpath(dirname(__DIR__)));
}

$config = require_once(APPLICATION_PATH.'/config/global.php');

spl_autoload_register(function ($class) {
    require_once APPLICATION_PATH.'/classes/'.$class.'.class.php';
});

require_once(APPLICATION_PATH.'/vendor/autoload.php');

session_start(); /* session start must be placed below class requires */

// Prepare app
$app = new \Slim\Slim($config['slim']);
require_once (APPLICATION_PATH.'/app/app.php');

// Run app
$app->run();