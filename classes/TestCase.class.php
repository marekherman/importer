<?php
/**
 * 
 */
class TestCase extends TestEntity{
    public $testSuite, $parentTestSuite;
    public $name, $summary, $preConditions, $importance;
    public $executionType;
    public $steps = array();
    public $stepExecutionType;
    public $coverageTitle, $coverageDocument;
    
    protected $stepNumber = 1;

    /**
     * Create a new test case
     * @param string $testCaseName
     */
    public function __construct($testCaseName=null) {
        $this->name = $testCaseName;
    }
    
    
    /**
     * @TODO if adding test cases to a test suite (not a test plan) is needed
     * @throws Exception
     */
    public function loadFromTestSuite(){
        throw new Exception("Method loadFromTestSuite not implemented.");
    }
    
    /**
     * Find test case in test plan with provided name
     * 
     * @global \Slim\Slim $app
     * @param string $planName
     * @throws Exception
     */
    public function loadFromTestPlan($planName){
        global $app;
        
        if( !is_null($this->name) && !is_null($planName) ){
            $testCase = $this->getTestCaseByPlan($planName);
            if( !is_array($testCase) ){
                throw new Exception("Test case with name '".$this->name."' was not found in plan '".$planName."' (project '".$app->form->values['project']->name."')");
            }
        }
        foreach( $testCase as $key=>$value ){
            if( !empty($value) ){
                $this->$key = $value;
            }
        }
    }
    
    
    /**
     * Find test case in test plan with provided name
     * 
     * @global \Slim\Slim $app
     * @param string $planName
     * @param int $testPlanId
     * @returns array
     */
    public function getTestCaseByPlan($planName){
        global $app;
        /** We need to know test plan because there can be two test cases
         * with the same name in different projects (which API doesn't solve) */
        
        /** Find test plan with given name (as we need ID later): */
        $plan = new TestPlan;
        $plan->getTestPlanByName($planName);
        $this->plan = $plan;
        
        /* Ask for all test cases in given test plan: */
        $api = new Api;
        $args = array( 'testplanid'=> $this->plan->id );
        $results = $api->query("getTestCasesForTestPlan", $args, $app->config('debug') );

        foreach($results as $details){
            $result = $details[0];
            if( array_key_exists("code", $result) && $result['code']=="5030" ){
                throw new Exception("Test case ".$this->name." was not found in plan ".$planName);
            } else if( array_key_exists("code", $result) && $result['code']=="3031" ){
                throw new Exception("No builds detected! ".$result['message']);
            } else if($result['tcase_name']==$this->name){
                return $result; /* check because there can be test cases with identical names in different plans */
            }
        }
        throw new Exception("Invalid input provided! Test case: '".$this->name."' was not found in test plan: '".$planName."'");
    }
    
    /**
     * Adds step to the test case
     * 
     * @param string $stepName
     * @param string $expectedResult
     * @param int $stepExecutionType
     */
    public function addStep($stepName, $expectedResult, $stepExecutionType=1){
        $this->steps[] = array(
            'step_number'           => $this->stepNumber,
            'actions'               => $stepName,
            'expected_results'      => $expectedResult,
            'execution_type'        => $stepExecutionType
        );
        $this->stepNumber++;
    }
    
}
