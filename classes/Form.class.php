<?php
/**
 * 
 */
class Form{
    /**
     *
     * @var $values['entityType'] enum('requirements','tests','testsresults') 
     * @var $values['project'] \TestProject
     */
    public $values = array(
        "fileName"          => null,
        "originalFileName"  => null,
        "project"           => null,
        "entityType"        => null,
        "hasHeaders"        => null
    );
    
    public function __construct(){
        if( isset($_SESSION['form']) ){
            foreach( $this->values as $key=>$value ){
                $this->values[$key] = $_SESSION['form'][$key];
            }
        }
    }
    
    /**
     * GET request api-connect
     * Render form for API information
     * 
     * @global \Slim\Slim $app
     */    
    public function getApiConnect(){
        global $app;
        $app->render('api-connect.php',
           array(   'projects' => $app->api->projects,
                    'url'      => $app->api->url )
        );
    }
    
    /**
     * POST request api-connect
     * Check if API connection works
     * 
     * @global \Slim\Slim $app
     */    
    public function postApiConnect(){
        global $app;
        try {
            $post = $app->request->post();
            $author = (empty($post['author'])) ? "admin" : $post['author'];
            $app->api->checkConnection($post['url'], $post['apikey'], $author);
        } catch (Exception $ex) {
            $app->flashNow('error', $ex->getMessage() );
            $app->render('api-connect.php');
            exit();
        }
        $app->redirect('file-upload');
    }
    
    /**
     * GET request file-upload
     * Render form for file upload
     * 
     * @global \Slim\Slim $app
     */
    public function getFileUpload(){
        global $app;
        $app->render('file-upload.php',
           array(   'filesize' => $app->config('files.size'),
                    'fileName' => $app->form->values['originalFileName'],
                    'projects' => $app->api->projects,
                    'url'      => $app->api->url )
        );
    }
    
    /**
     * POST request for file-upload
     * Save uploaded file
     * 
     * @global \Slim\Slim $app
     * @return boolean
     * @throws Exception
     */
    public function postFileUpload(){
        global $app;
        try{
            $post = $app->request->post();
            $entityType = array_key_exists("entityType", $post) ? $post['entityType'] : null;
            $this->setOriginalFileName  ($_FILES['file']['name']);
            $this->setFileName          ($_FILES['file']['name']); /* method creates a new unique name */
            $this->setEntityType        ($entityType); /* type of uploaded file */
            $this->setProject           ($post['projectId']);
            $this->setHeaders           (array_key_exists("hasHeaders", $post) ? true : false); /* file headers (ignore first row) */

            copy($_FILES['file']['tmp_name'], $app->config('temp.files')."/".$this->values['fileName']);
            $this->saveData();
        } catch (Exception $ex){
           $app->flash('error', $ex->getMessage());
           $app->flashKeep();
           $app->redirect('file-upload');
        }
        $app->redirect('file-process');
    }
    
    /**
     * 
     * Reads contents of uploaded file and presents to user for confirmation
     * (without importing them)
     * 
     * @global \Slim\Slim $app
     */
    public function readUploadedFile(){
        global $app;
        try {
            $fileName = APPLICATION_PATH.'/temp/'.$this->values['fileName'];
            $file = $this->getFileType($fileName);
            $data = $file->processFile();
            $app->render('file-report-'.$this->values['entityType'].'.php',
               array('data' => $data, 'project'=>$this->values['project'])
            );
        } catch (Exception $ex){
           $app->flash('error', $ex->getMessage());
           $app->flashKeep();
           $app->redirect('file-upload');
        }
    }
    
    /**
     * Returns file class based on file contents saved in $this->values['entityType']
     * 
     * @param string $fileName
     * @return extends File
     */
    public function getFileType($fileName){
        switch($this->values['entityType']){
            case "requirements":
                $file = new FileTestRequirement($fileName);
            break;
            case "tests":
                $file = new FileTestCase($fileName);
            break;
            case "testsresults":
                $file = new FileTestResult($fileName);
            break;
        }
        $file->project = $this->values['project'];
        $file->setHeaders($this->values['hasHeaders']);
        return $file;
    }
    
    /**
     * Imports contents of the file into TestLink
     * 
     * @global \Slim\Slim $app
     */
    public function import(){
        global $app;
        $post = $app->request->post();
            
        $fileName = APPLICATION_PATH.'/temp/'.$this->values['fileName'];
        $file = $this->getFileType($fileName);
        $file->setConfirmed($post['confirmed']); /* rows confirmed by user on previous screen to be imported: */ 
        
        $file->processFile();
        $log = $file->importFile();
        $app->render('import.php',
           array('log' => $log)
        );
    }
    
    /**
     * Save data from form into session
     */
    public function saveData(){
        foreach( $this->values as $key=>$value ){
            $_SESSION['form'][$key] = $value;
        }
    }
    
    /**
     * Sets current project based on provided ID
     * 
     * @param int $projectId
     * @throws Exception
     */
    public function setProject($projectId){
        if( empty($projectId) || !is_numeric($projectId) ){
            throw new Exception("Project ID must be a valid number");
        }
        $project = new TestProject();
        $project->loadFromSession($projectId);
        $this->values['project'] = $project;
    }
    
    /**
     * Sets entity type (i.e. what user wants to import)
     * 
     * @param string $entityType
     * @throws Exception
     */
    public function setEntityType($entityType){
        if( empty($entityType) || !in_array($entityType, array("testsresults","tests","requirements") )){
            throw new Exception("Please choose the type of file you want to upload (requirements, test cases or results).");
        }
        $this->values['entityType'] = $entityType;
    }
    
    /**
     * Sets original name of file uploaded by user
     * 
     * @param string $fileName
     * @throws Exception
     */
    public function setOriginalFileName($fileName){
        if( empty($fileName) || !is_string($fileName) ){
            throw new Exception("You did not provide any file.");
        }
        $this->values['originalFileName'] = $fileName;
    }
    
    /**
     * Sets new unique name of file uploaded by user
     * 
     * @param string $fileName
     */
    public function setFileName($fileName){
        $this->values['fileName'] = pathinfo($fileName, PATHINFO_FILENAME)."_".time().".".pathinfo($fileName, PATHINFO_EXTENSION);
    }
    
    /**
     * Set if file has headers (ignore first row)
     * 
     * @param boolean $headers
     */
    public function setHeaders($headers){
        $this->values['hasHeaders'] = $headers;
    }
}