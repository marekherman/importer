<?php
/**
 * 
 */

class FileTestCase extends File{
    /**
     * Order of columns imported from a File (e.g. CSV or XLS)
     * @var array
     */
    protected static $importOrder = array(
        "testSuiteName" => 0, /* required for test case */
        "testSuiteDetails" => 1,
        "testCaseNumber" => 2,
        "name" => 3, /* required for test case */
        "summary" => 4,
        "preConditions" => 5,
        "executionType" => 6,
        "importance" => 7, /* required for test case */
        "customField1" => 8,
        "customField2" => 9,
        "stepName" => 10, /* required for step */
        "expectedResult" => 11, /* required for step */
        "stepExecutionType" => 12,
        "coverageTitle" => 13,
        "coverageDocument" => 14 /* required for step */
    );
    
    protected $lastRowType;
    protected $currentTestSuite = null;
    protected $currentTestCaseName = null;
    protected $testCases = array();
    

    /**
     * Determines type of provided row (test suite/test case/test step)
     * 
     * @param array $row
     * @param int $rowNumber
     * @throws Exception
     * @return enum("suite","case","step")
     */
    private function getRowType($row,$rowNumber=null){
        $rowType = false;
        $suiteOrder     = static::$importOrder["testSuiteName"];
        $caseOrder      = static::$importOrder["name"];
        $stepOrder      = static::$importOrder["stepName"];
        
        if( !empty($row[$caseOrder]) && array_key_exists($row[$caseOrder], $this->testCases) ){
            throw new Exception("Test cases with identical names (".$row[$caseOrder].") found in your document. Test case name must be unique");
        }
        switch (true) {
            case !empty($row[$suiteOrder]) && empty($row[$caseOrder]) && empty($row[$stepOrder]):
                $rowType = "suite"; break;
            case !empty($row[$caseOrder]) && !empty($row[$stepOrder]):
                $rowType = "case"; break;
            case empty($row[$suiteOrder]) && empty($row[$caseOrder]) && !empty($row[$stepOrder]):
                $rowType = "step"; break;
        }
        if( $rowType===false && array_filter($row)  ){
            throw new Exception("Importing test cases failed. Row with unexpected formatting was found in row #".$rowNumber.". Please refer to the example.");
        }
        return $rowType;
    }
    

    /**
     * Process a row containing one test case step
     * 
     * @param array $row Row of the imported excel file
     */
    private function processTestStep($row){
        $nameOrder          = static::$importOrder["stepName"];
        $resultOrder        = static::$importOrder["expectedResult"];
        $typeOrder          = static::$importOrder["stepExecutionType"];
        $this->testCases[$this->currentTestCaseName]->addStep($row[$nameOrder],$row[$resultOrder],$row[$typeOrder]);
    }
    
    /**
     * Process a row containing a test suite
     * 
     * 
     * @param array $row Row of the imported excel file
     */
    private function processTestSuite($row){
        $parentId = false;
        if ( !is_null($this->currentTestSuite) && $this->lastRowType=="suite" ){
            $testSuite = clone $this->currentTestSuite;
            $this->currentTestSuite->parent = $testSuite;
            $parentId = $this->currentTestSuite->parent->id;
        }
        $name       = $row[static::$importOrder['testSuiteName']];
        $details    = $row[static::$importOrder['testSuiteDetails']];
        $this->currentTestSuite = new TestSuite($name, $this->project, $details, $parentId);
    }
    
    /**
     * Process a row containing a test case
     * 
     * @param array $row Row of the imported excel file
     */
    private function processTestCase($row){
        $testCase = new TestCase;
        foreach( static::$importOrder as $key=>$value ){
            if( !empty($row[$value]) ){
                $testCase->$key = $row[$value];
            }
        }
        $this->currentTestCaseName = $testCase->name;
        $testCase->addStep($testCase->stepName,$testCase->expectedResult,$testCase->stepExecutionType);
        $testCase->testSuite        = $this->currentTestSuite;
        $this->testCases[$testCase->name] = $testCase;
    }
    

    /**
     * Parse through uploaded file and process each line accordingly
     * Returns an array of test cases found in the file
     * 
     * @return array \TestCase
     */
    public function processFile(){
        $data = $this->getData();
        $m=0;
        foreach ($data as $row) {
            $m++;
            $rowType = $this->getRowType($row,$m);
            switch($rowType){
                case "suite":   $this->processTestSuite($row);  break;
                case "case":    $this->processTestCase($row);   break;
                case "step":    $this->processTestStep($row);   break;
            }
            $this->lastRowType = $rowType;
        }
        return $this->testCases;
    }
    

    /**
     * Import processed file into the Testlink installation
     * Returns array of logs for each row imported
     * 
     * @return array
     * @global \Slim\Slim $app
     */
    public function importFile(){
        global $app;
        
        $api = new Api;
        $logs = array();
        foreach( $this->testCases as $testCase ){
            if( !in_array($testCase->name, $this->confirmed) ){
                $logs[] = "Ignoring test case ".$testCase->name." as requested";
                continue;
            }
            $args = array(
                'testcasename'          => $testCase->name,
                'testsuiteid'           => $testCase->testSuite->id,
                'testprojectid'         => $this->project->id,
                'authorlogin'           => $api->author,
                'summary'               => $testCase->summary,
                'steps'                 => $testCase->steps,
                'preconditions'         => $testCase->preConditions,
                'importance'            => $testCase->importance,
                'execution'             => $testCase->executionType,
                'checkduplicatedname'   => '1'
            );
            $result = $api->query("createTestCase", $args, $app->config('debug') );
            $case = $result[0];
            $log = "".$testCase->name." adding into suite ".$testCase->testSuite->name.": ";
            if( array_key_exists('code', $case) && $case['code']==10000 ){
                $log .= $case['message'];
            } else if( array_key_exists('additionalInfo', $case) && $case['additionalInfo']['status_ok']==1 ){
                $log .= "OK! ".$case['additionalInfo']['msg'];
            }
            $logs[] = $log;
        }
        return $logs;
    }
    
    
    /**
     * Convert file into XML format
     * @return string
     */
    public function toXML(){
    }
    
}