<?php
class TestProject extends TestEntity{
    public $id = null;
    public $name = null;
    public $options = null;


    public function __construct() {
    }
    
    /**
     * Sets project attributes based on provided project ID
     * 
     * @param int $id
     * @throws Exception
     * @return boolean
     */
    public function loadFromSession($id){
        if( !isset($_SESSION['api']['projects']) ){
            throw new Exception("Project couldn't be created due to missing information. Maybe you deleted your cookies. Please try connecting again");
        }
        foreach( $_SESSION['api']['projects'] as $project ){
            if( $project['id']==$id ){
                foreach( $project as $key=>$value ){
                    $this->$key = $value;
                }
                return true;
            }
        }
        return false;
    }
    
    /**
     * Set project name
     * 
     * @param string $name
     */
    public function setName($name){
        $this->name = $name;
    }
    

}