<?php
/**
 * 
 */


class FileTestResult extends File{
    
    /**
     * Order of columns imported from a File (e.g. CSV or XLS)
     * @var array
     */
    protected static $importOrder = array(
        "testPlanName"  => 0, /* required */
        "testName"      => 1, /* required */
        "buildId"       => 2, /* required */
        "status"        => 3, /* i.e. test result */
        "notes"         => 4,
        "tester"        => 5,
        "timestamp"     => 6,
        "bugId"         => 7
    );

    public $testResults = array();
    
    
    /**
     * Parse through uploaded file and process each line accordingly
     * Returns an array of test results found in the file
     * 
     * @return array \TestResult
     */
    public function processFile(){
        $data = $this->getData();
        $m=0;
        foreach ($data as $row) {
            $m++;
            $this->processRow($row,$m);
        }
        return $this->testResults;
    }
    
    /**
     * Process one row of provided file
     * 
     * @param array $row
     * @param int $m
     * @throws Exception
     */
    public function processRow(array $row,$m){
        $plan   =   $row[static::$importOrder['testPlanName']];
        $case   =   $row[static::$importOrder['testName']];
        if( empty($plan) || empty($case) ){
            throw new Exception("Importing test results failed. Row ".$m." has unexpected formatting. You must provide test case name and test suite name.");
        }
        $testResult = new TestResult;
        foreach( static::$importOrder as $key=>$value ){
            if( !empty($row[$value]) ){
                $testResult->$key = $row[$value];
            }
        }
        /* Fix timestamp if Excel incorrectly converted it: */
        $testResult->timestamp = date_format(date_create($testResult->timestamp), "Y-m-d H:i:s");
        /* set user who did the test (if not set, admin is used) */
        $testResult->tester = (empty($testResult->tester) ? "admin" : $testResult->tester);
        $this->testResults[] = $testResult;
    }
    
    /**
     * Import processed file into the Testlink installation
     * Loop through detected test results and try to import each of them
     * 
     * @global \Slim\Slim $app
     * @return array
     */
    public function importFile(){
        global $app;
        $logs = array();
    try {
        foreach( $this->testResults as $testResult ){
            $testCase = new TestCase($testResult->testName);
            $testCase->loadFromTestPlan($testResult->testPlanName);
            $buildId = (empty($testResult->buildId)) ? $testCase->plan->build->id : $testResult->buildId; /* if buildId is empty, use latest build */
            
            $args = array(
                'testcaseid'            => $testCase->tcase_id,
                'testplanid'            => $testCase->plan->id,
                'testcaseexternalid'    => $testCase->full_external_id,
                'buildid'               => $buildId,
                'testprojectid'         => $this->project->id,
                'status'                => $testResult->status,
                'notes'                 => $testResult->notes,
                'user'                  => $testResult->tester,
                'overwrite'             => $app->config('overwrite.testresults')
            );
            $api = new Api;
            $outs = $api->query("reportTCResult", $args, $app->config('debug') );
            $out = $outs[0];
            $log = "Importing result of '".$testResult->testName."' test in plan '".$testCase->plan->name."': ";
            if( array_key_exists('code', $out) && $out['code']==10000 ){
                $log .= $out['message'];
            } else if( array_key_exists('code', $out) && $out['code']==3031 ){
                $log .= "Your test plan cannot be used for import: ".$out['message'];
            } else if( array_key_exists('code', $out) && $out['code']==3032 ){
                $log .= "Specified build ID (".$testResult->buildId.") invalid. Please provide valid ID or leave blank for latest: ".$out['message'];
            } else if( $out['status']==true ){
                $log .= "OK! ".$out['message']." (overwrite ".var_export($out['overwrite'],true).")";
            }
            $logs[] = $log;
        }
    } catch (Exception $ex){
        $app->flash('error', $ex->getMessage());
        $app->flashKeep();
        $app->redirect('file-process');
    }
        return $logs;
    }
    
    /**
     * Convert file into XML format
     * @return string
     */
    public function toXML(){
        
    }
}
