<?php

class TestSuite extends TestEntity{
    /**
     * @var string name and details of the test suite 
     */
    public $name, $details;
    /**
     * @var \TestProject project to which the test suite belongs
     */
    public $project;
    /**
     *
     * @var \TestSuite parent test suite
     */
    public $parent = null;
    public $created = false;
    public $id, $parent_id, $node_type_id, $node_order, $node_tabletestsuites, $namesupersuite = null;
    
    /**
     * Create a test suite and create it via API if it doesn't exist
     * 
     * @global \Slim\Slim $app
     * @param string $name
     * @param TestProject $project
     * @param string $details
     * @param int $parentSuiteId
     * @throws Exception
     * @return boolean
     */
    public function __construct($name, $project, $details=false, $parentSuiteId=false) {
        global $app;
        if( !strlen($name) || !$project instanceof TestProject ){
            throw new Exception("You must provide a test suite name and project to which it belongs");
        }
        $this->name = $name;
        $this->project = $project;
        $this->details = $details;
        
        /** Get details about the test suite from API */
        $result = $this->getSuiteByName($this->name, $this->project->prefix);
        if(is_array($result) ){
            /* Get an existing test suite: */
            foreach( $result[0] as $key=>$value ){
                $this->$key = $value;
            }
            return true;
        }
        if( $result==false && $app->config('createNonexistentSuites') ){
            /* Create a new test suite because it doesn't exist: */
            $data = $this->createTestSuite($this->name, $this->details, $this->project->id, $parentSuiteId);
            if( is_array($data) && $data[0]['message']=='ok' ){
                $this->id = $data[0]['id'];
                $this->created = true;
                return true;
            } else {
                throw new Exception("Creation of suite ".$this->name." failed: ".$data[0]['message']);
            }
        } else if($result==false){
            /* Test suite doesn't exist and creating nonexistent test suites is disallowed in config */
            throw new Exception("There was no Test Suite found called ".$this->prefix."-".$this->name." in your TestLink.".
                    "Please allow creation of non-existent test suite or create it yourself first.");
        }
    }
    
    /**
     * Creates a new test suite via API
     * 
     * @global \Slim\Slim $app
     * @param string $name
     * @param string $details
     * @param int $projectId
     * @param int $parentSuiteId
     * @return array
     */
    public function createTestSuite($name, $details, $projectId, $parentSuiteId=false){
        global $app;
        $args = array(
            "testsuitename"         => $name,
            "details"               => $details,
            "testprojectid"         => $projectId
        );
        if( $parentSuiteId ){
            $args["parentid"] = $parentSuiteId;
        }
        $api = new Api;
        $testSuite = $api->query("createTestSuite", $args, $app->config('debug'));
        return $testSuite;
    }
    
    /**
     * Sets project to which the test suite belongs
     * 
     * @param \TestProject $project
     * @throws Exception
     */
    public function setProject($project){
        if( !$project instanceof \TestProject ){
            throw new Exception("Project of a test suite must be a TestProject object.");
        }
        $this->project = $project;
    }
    
    public function setDetails($details){
        $this->details = $details;
    }
    
    public function setParent($parent){
        $this->parent = $parent;
    }
    
    public function hasParent(){
        return ( is_null($this->parent) ) ? false : true;
    }
    
    /**
     * Finds test suite with given name in a project specified by prefix.
     * 
     * @global type $app
     * @param string $name
     * @param string $prefix
     * @return mixed
     * @throws Exception
     */
    public function getSuiteByName($name, $prefix){
        global $app;
        $api = new Api;
        $args = array(
            "prefix"            => $prefix,
            "testsuitename"     => $name
        );
        $testSuite = $api->query("getTestSuite", $args, $app->config('debug'));
        if( empty($testSuite) ){
            return false;
        }
        return $testSuite;
    }
    
}