<?php
abstract class File{

    /**
     * A file read by the import tool
     * 
     * @var PHPExcel_Reader_IReader file
     * @var string fileType
     * @var string fileName 
     */
    protected $file;
    protected $extension;
    protected $fileType;
    protected $fileName;
    protected $hasHeaders;
    public $project;
    
    public $confirmed = array();
        
/**
 * 
 * @param PHPExcel_Reader_IReader $file
 * @global \Slim\Slim $app
 */
    public function __construct($file=false) {
        global $app;
        $this->fileName         = $file;
        $this->file             = $file;
        $this->fileType         = $this::getFileType($this->fileName);
        
        try {
            $objReader = PHPExcel_IOFactory::createReader($this->fileType);
            $objPHPExcel = $objReader->load($this->fileName);
            $this->file = $objPHPExcel;
        } catch(Exception $e) {
           $app->flash('error', 'Uploaded file is corrupted or not readable: '.$e->getMessage());
           $app->flashKeep();
           $app->redirect('file-upload');
       }
    }
    

    /**
     * 
     * @param string $fileName
     * @return string
     * @throws Exception
     */
    public static function getFileType($fileName){
        $fileNameExploded = explode(".", $fileName);
        $extension = array_pop($fileNameExploded);
        
        $fileType = false;
        switch( $extension ){
            case "xls":     $fileType = "Excel5"; break;
            case "xlsm":    $fileType = "Excel5"; break;
            case "xlsx":    $fileType = "Excel2007"; break;
            case "xml":     $fileType = "Excel2003XML"; break;
            case "csv":     $fileType = "CSV"; break;
        }
        if(!$fileType){ throw new Exception("Invalid file submitted. Supported extensions are XLS, XLSX, XML, CSV"); }
        return $fileType;
    }
    
    
    /**
     * Prepare active sheet of the Excel file for reading
     * @return \PHPExcel_Worksheet
     * @throws Exception
     */
    public function getSheet(){
        $sheets     = $this->file->getSheetNames();
        $sheet      = $this->file->getActiveSheet();
        if( get_class($sheet)!="PHPExcel_Worksheet" ){
            throw new Exception("Active sheet could not be read. All found sheets: ".implode(", ",$sheets)."");
        }
        return $sheet;
    }
    
    /**
     * Gets data from active sheet
     */
    public function getData(){
        $sheet = $this->getSheet();
        $startCell = ($this->hasHeaders) ? "A2" : "A1";
        $maxCell    = $sheet->getHighestRowAndColumn();
        $data       = $sheet->rangeToArray($startCell. ':' . $maxCell['column'] . $maxCell['row']);
        return $data;
    }
    
    /**
     * First row will be ignored if file is set to have headers
     * @param boolean $hasHeaders
     */
    public function setHeaders($hasHeaders=false){
        $this->hasHeaders       = $hasHeaders;
    }
    
    /**
     * Sets which rows of the file are confirmed by user to be imported.
     * User can uncheck the rows so that they are then unconfirmed.
     * @param array $confirmed
     */
    public function setConfirmed(Array $confirmed){
        foreach( $confirmed as $name ){
            if(strlen($name)!=0 ){
                $this->confirmed[] = $name;
            }
        }
    }
    
    /**
     * Overloaded by child classes
     */
    abstract public function processFile();
    abstract public function importFile();
    abstract public function toXML();
    
}
