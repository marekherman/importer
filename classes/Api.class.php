<?php
class Api{
    protected $apiKey   = false;
    public $url         = false;
    public $author      = null;
    public $projects    = false;
    public $version     = null;
    public $debug       = false;
    
    /**
     * Methods that must be supported by API on the server
     */
    private $requiredMethods = array("getTestSuite","createRequirement");//,"getRequirementByID");
    /**
     * Explanation of unsupported Testlink
     */
    private $unsupportedVersionMessage = "You are probably using an older or unsupported version of TestLink.
        Please check the latest version of xmlrpc.class.php (see our FAQ section).";
    
    /**
     * 
     * @global \Slim\Slim $app
     */
    public function __construct(){
        global $app;
        $this->debug = $app->config('debug');
        if( isset($_SESSION['api']) ){
            foreach( $_SESSION['api'] as $key=>$value ){
                $this->$key = $value;
            }
        }
    }
    
    /**
     * Method connects to the API and queries $query with $args arguments
     * 
     * @global \Slim\Slim $app
     * @param string $query
     * @param array $args
     * @param boolean $debug
     * @return array
     * @throws \Exception
     */
    public function query($query, $args = array(), $debug=null){
        global $app;
        $client = new IXR_Client($this->url);
        $client->debug = (is_null($debug)) ? $this->debug : $debug;
        $args['devKey'] = $this->apiKey;
        try{
            $queryResult = $client->query("tl.{$query}", $args );
            if(!$queryResult && !$client->debug){
               return false; 
            }
            switch(true){
                case (!$queryResult && $client->error->code=="-32700" && $query=="createRequirement" ):
                    return $client->getResponse(); /* ignore this error because it is badly implemented in API */
                case (!$queryResult && $client->error->code=="-32700" && $query!="createRequirement" ):
                    throw new Exception ( var_export($client,true)." You are probably using an incompatible version of Testlink. " );
                case (!$queryResult && $client->error->code=="-32601" && ($query=="getTestSuite" || $query=="createRequirement") ):
                    throw new Exception ($this->unsupportedVersionMessage);
                case (!$queryResult && $client->error->code=="-32601"):
                    throw new Exception (var_export($client,true).$this->unsupportedVersionMessage);
                case (!$queryResult):
                    throw new Exception ( $client->error->code.": ".$client->error->message );
            }
        } catch (Exception $ex) {
           $app->flashNow('error', 'Query failed: '.$ex->getMessage());
           $app->render('api-connect.php');
           exit();
        }
        return $client->getResponse();
    }
    
    /**
     * Method checks connection and stores API key and API URL into session
     * 
     * @param string $url
     * @param string $apiKey
     * @returns boolean
     */
    public function checkConnection($url, $apiKey, $author="admin"){
        $this->url = $url;
        $this->apiKey = $apiKey;
        $msg = false;
        
        $connection = $this->query("checkDevKey"); /* correct response = true (boolean) */
        if ($connection===false){
            $msg = "Connection couldn't be established.";   
        }
        if( substr($url,-16)=="xmlrpc.class.php" ){
            $msg = "Your API location must end with xmlrpc.php, not xmlrpc.class.php";
        }
        if( is_array($connection) ){
          switch($connection[0]['code']){
              case "2000":    $msg = "Your developer (API) key is invalid."; break;
              case "100":     $msg = "Developer (API) key was not provided"; break;
              default:        $msg = $connection[0]['message']; break;
          }
        }
        $version    = $this->getTestLinkVersion();
        $projects   = $this->getProjects();
        $this->checkSupportedMethods($this->requiredMethods);
        
        if ( $connection===true && $projects===false ){
            $msg = "Connection was successful but there are no projects in your TestLink installation.";
        }
        if( $msg!==false ){
            throw new Exception($msg);
        } else {
            $_SESSION['api'] = array('apiKey'=>$apiKey, 'author'=>$author, 'url'=>$url, 'projects'=>$projects, 'version'=>$version);
            return true;
        }
    }
    
    /**
     * Non-complete function to check if methods do exist in the API
     * 
     * @global \Slim\Slim $app
     * @param array $methods
     * @return type
     */
    public function checkSupportedMethods(array $methods){
        global $app;
        $tests = array();
        foreach($methods as $method){
            $args = array();
            $tests[] = $this->query($method, $args, $app->config('debug'));
        }
        return $tests;
    }
    
    
    /**
     * Returns projects from a TestLink installation
     * 
     * @return mixed
     */
    public function getProjects(){
        $projects = $this->query("getProjects");
        if( count($projects)==0 ){
            return false;
        }
        return $projects;
    }
    
    /**
     * Returns TestLink version
     * @return string
     * @throws Exception
     */
    public function getTestLinkVersion(){
        global $app;
        
        $version = $this->query("testLinkVersion");
        $supportedVersion = $app->config('api.supportedVersion'); /* from config/global.php */
        if( version_compare($version, $supportedVersion)==-1 ){
            throw new Exception("Your TestLink version is ".$version.". Sorry but only versions ".$supportedVersion." and higher are supported");
        }
        return $version;
    }
    
}