<?php
/**
 * 
 */
class Validator{
    
    /**
     * Check input before attempting to connect
     * 
     * @global \Slim\Slim $app
     */
    public static function sanitizeApi(){
        global $app;
        $data = $app->request()->post();
        if( filter_var($data['url'], FILTER_VALIDATE_URL)===false || empty($data['apikey']) ){
            $app->flash('error', 'Invalid input provided. Please enter a valid API address (URL), valid API key and valid username.');
            $app->flashKeep();
            $app->redirect('api-connect');
        }
    }
        /**
     * Check input before attempting to process file
     * 
     * @global \Slim\Slim $app
     * @throws Exception
     */
    public static function sanitizeFile(){
        global $app;
        try{
            File::getFileType($_FILES["file"]["name"]);
            if( $_FILES['file']==0 || $_FILES['file']>400000 ){
                throw new Exception("File should be smaller than 400 kB and not empty to properly upload into TestLink.");
            }
        } catch (Exception $ex) {
            $app->flashNow('error', $ex->getMessage() );
        }
    }
    
    
    /**
     * Check state of application before displaying some pages
     * 
     * @global \Slim\Slim $app
     * @throws Exception
     */
    public static function sanitizeProcess(){
        global $app;
        try{
            if( empty($app->api->url) || empty($app->api->projects) ){
                throw new Exception("Before uploading a file, please connect to your TestLink installation in the first step.");
            }
        } catch (Exception $ex) {
            $app->flash('error', $ex->getMessage() );
            $app->flashKeep();
            $app->redirect('api-connect');
        }
        
    }
    
    /**
     * Check state of server before displaying some pages
     * 
     * @global \Slim\Slim $app
     * @throws Exception
     */
    public static function checkFolders(){
        global $app;
        try{
            if( !is_writable($app->config('sessions.files')) ){
                throw new Exception("Please make your sessions folder (".$app->config('sessions.files').") writable. Try setting file permissions to 777.");
            }
            if( !is_writable($app->config('temp.files')) ){
                throw new Exception("Please make your temp folder (".$app->config('temp.files').") writable. Try setting file permissions to 777.");
            }
            if( !is_readable($app->config('templates.path')) ){
                throw new Exception("Please make your templates folder (".$app->config('sessions.files').") readable.");
            }
        } catch (Exception $ex) {
            $app->flashNow('error', $ex->getMessage() );
        }
    }
}