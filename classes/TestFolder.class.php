<?php
/**
 * TestFolder (i.e. Requirement specification)
 * 
 */

class TestFolder extends TestEntity{
    
    /**
     *
     * @var \TestFolder parent folder
     */
    public $parent = null;
    private $req_spec_type = 1; /* 1=Section, 2=User Req Spec, 3=System Req Spec */
    
    public function __construct($id=null) {
        $this->id   = $id;
    }
    

    /**
     * Get a folder (requirement specification) by the provided ID.
     * If $createNonExistingReqSpec (in config.php) is true and the folder doesn't exist, it will be created.
     * 
     * @global \Slim\Slim $app
     * @param string $id
     * @param boolean $external - true: external ID will be used; false: internal ID will be used
     * @return array
     */
    public function getFolderById($id, $external=true){
        global $app;
        $api = new Api;
        
        $args = array(
            "testprojectid"            => $app->form->values['project']->id
        );
        $specifications = $api->query("getRequirementSpecifications", $args, $app->config('debug') );
        if( $specifications==false && $app->config('createNonexistentReqSpecs')==false ){
            throw new Exception("There were no requirement specifications found in project ".$app->form->values['project']->name);
            
        } else if( $specifications!=false ){
            foreach($specifications as $specification){
                $idFieldName = ($external==true) ? "doc_id" : "id";
                if( $specification[$idFieldName]==$id ){
                    foreach( $specification as $key=>$value ){
                        if( !empty($value) ){
                            $this->$key = $value;
                        }
                    }
                    return true;
                }
            }
        }
        
        if( $app->config('createNonexistentReqSpecs')==false ){
            throw new Exception ("Requirement specification ID '".$id."' was not found in project '".$app->form->values['project']->name."'");
        } else {
            $specification = $this->createTestFolder($id);
            if(is_int($specification['id']) ){
                $this->getFolderById($specification['id'], false); /* run the function again with newly created (internal) ID */
            }
        }
    }
    
    /**
     * 
     * @global \Slim\Slim $app
     * @param string $id
     * @return type
     * @throws Exception
     */
    public function createTestFolder($id){
        global $app;
        
        $api = new Api;
            $args = array(
                "testprojectid" => $app->form->values['project']->prefix, /* project prefix (API needs prefix, not ID!) */
                "req_parent_id" => $app->form->values['project']->id, /* parent element ID */
                "title"         => $id, /* requirement specification title */
                "document_id"   => $id, /* requirement specification external ID */
                "user_id"       => $api->author, /* username */
                "req_spec_type" => $this->req_spec_type, 
                "scope"         => "", /* not implemented */
//                "count_req"     => "", /* count of requirements, not implemented */
//                "order"         => ""  /* sorting order, not implemented */
            );
        $reqSpec = $api->query("createReqSpec", $args, $app->config('debug'));
        if( $reqSpec["status_ok"]==0 ){
            throw new Exception("Requirement specification could not be created: ".$reqSpec['msg']);
        }
        return $reqSpec;
    }
    
    public function getParent(){
        return $this->parent;
    }
    
    public function setParent($parent){
        $this->parent = $parent;
    }
    
    public function hasParent(){
        return ( is_null($this->parent) ) ? false : true;
    }
}