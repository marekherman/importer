<?php
class TestPlan extends TestEntity{
    public $testProject;
    
    
    public function __construct() {
        global $app;
        $this->testProject = new TestProject();
        $this->testProject->setName($app->form->values['project']->name);
    }
    
    /**
     * Gets test plan with given name from API
     * 
     * @global \Slim\Slim $app
     * @param string $name
     * @return array
     * @throws Exception
     */
    public function getTestPlanByName($name){
        global $app;
        $args = array(
            "testplanname"         => $name,
            "testprojectname"      => $this->testProject->name
        );
        $api = new Api;
        $testPlans = $api->query("getTestPlanByName", $args, $app->config('debug'));
        $testPlan = $testPlans[0]; /* max one result is possible (test plan name is unique) */
        if( array_key_exists("code", $testPlan) && $testPlan['code']=="3033" ){
            throw new Exception($testPlan['message']);
        }
        /* save test plan attributes: */
        foreach( $testPlan as $key=>$value ){
            if( !empty($value) ){
                $this->$key = $value;
            }
        }
        $this->build = $this->getLatestBuild();
        return $testPlan;
    }
    
    /**
     * Gets latest build of the test plan
     * 
     * @global \Slim\Slim $app
     * @return \TestBuild
     */
    public function getLatestBuild(){
        global $app;
        
        $api = new Api;
        $args = array('testplanid'=>$this->id);
        $buildResult = $api->query("getLatestBuildForTestPlan", $args, $app->config('debug'));
        $build = new TestBuild;
        /* save test build attributes: */
        foreach($buildResult as $key=>$value){
            if( !empty($value) ){
                $build->$key = $value;
            }
        }
        return $build;
    }
    
}