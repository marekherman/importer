<?php
/**
 * 
 */

class FileTestRequirement extends File{
    /**
     * Order of values imported from a File (e.g. CSV or XLS)
     * @var array
     */
    protected static $importOrder = array(
        "folderId"          => 0,
        "id"                => 1,
        "title"             => 2,
        "node_order"        => 3,
        "description"       => 4,
        "status"            => 5,
        "type"              => 6,
        "expectedCoverage"  => 7,
        "scope"             => 8
    );
    
    protected $requirements = array();
    protected $currentFolder = null;

    
    /**
     * Process file which consists of requirement rows and folder (req spec) rows
     * 
     * @global \Slim\Slim $app
     * @return array \Requirement
     * @throws Exception
     */
    public function processFile(){
        global $app;
        if( $app->form->values['project']->opt['requirementsEnabled']!=1 ){
            throw new Exception("You cannot import requirements into a project with disabled Enhanced features -> Requirements.");
        }
        $data = $this->getData();
        $m=0;
        foreach ($data as $row) {
            $m++;
            $rowType = $this->getRowType($row,$m);
            switch($rowType){
                case "folder":          $this->processFolder($row,$m);  break;
                case "requirement":     $this->processRequirement($row);   break;
            }
        }
        return $this->requirements;
    }
    
    
    /**
     * Process row of the file containing a folder (a requirement specification)
     * @param array $row
     */
    public function processFolder(array $row, $m=null){
        $idOrder            = static::$importOrder["folderId"];
        $titleOrder         = static::$importOrder["title"];
        if( empty($row[$idOrder]) || !empty($row[$titleOrder]) ){
            throw new Exception("Unexpected formating of folder (requirement specification) on row ".$m);
        }
        $folder = null;
        if ( !is_null($this->currentFolder) ){
            $folder = clone $this->currentFolder;
        }
        $this->currentFolder = new TestFolder();
        $id     = $row[static::$importOrder['folderId']];
        $this->currentFolder->getFolderById($id);
        
        if( !is_null($folder) ){
            $this->currentFolder->parent = $folder;
        }
    }
    
    /**
     * Process a row containing a test requirement
     * @param array $row Row of the imported excel file
     */
    public function processRequirement(array $row){
        $requirement = new TestRequirement;
        foreach( static::$importOrder as $key=>$value ){
            if( !empty($row[$value]) ){
                $requirement->$key = $row[$value];
            }
        }
        $requirement->node_order        = (isset($requirement->node_order)) ? $requirement->node_order : 1;
        $requirement->expectedCoverage  = (isset($requirement->expectedCoverage)) ? $requirement->expectedCoverage : 1;
        $requirement->scope             = (isset($requirement->scope)) ? $requirement->scope : "";
        $requirement->folder            = $this->currentFolder;
        $this->requirements[$requirement->title] = $requirement;
    }
    
    /**
     * Determines what does a row in uploaded file describe
     * 
     * @param array $row
     * @param int $m
     * @return string
     * @throws Exception
     */
    public function getRowType(array $row,$m=null){
        $rowType = false;
        $folderIdOrder      = static::$importOrder["folderId"];
        $titleOrder         = static::$importOrder["title"];
        $idOrder            = static::$importOrder["id"];
        switch (true) {
            case (!empty($row[$folderIdOrder])):
                $rowType = "folder";        break;
            case !empty($row[$titleOrder]) && !empty($row[$idOrder]):
                $rowType = "requirement";   break;
            default:
                throw new Exception("Unexpected formatting found on row ".$m.". Please refer to the sample files for correct formatting.");
        }
        return $rowType;
    }
    
    /**
     * Loop through found requirements and try to import them via API
     * 
     * @global \Slim\Slim $app
     * @return array
     */
    public function importFile( ){
        global $app;
        
        $api = new Api;
        $logs = array();
        foreach( $this->requirements as $requirement ){
            if( !in_array($requirement->title, $this->confirmed) ){
                $logs[] = "Ignoring requirement ".$requirement->title." as requested";
                continue;
            }
            $args = array(
                'testprojectid'         => $this->project->id,
                'req_spec'              => $requirement->folder->id,
                'document_id'           => $requirement->id,
                'title'                 => $requirement->title,
                'scope'                 => $requirement->scope,
                'user_id'               => $api->author,
                'reqStatus'             => $requirement->status,
                'reqType'               => $requirement->type,
                'expected_coverage'     => $requirement->expectedCoverage,
                'order'                 => $requirement->node_order,
            );
            $result = $api->query("createRequirement", $args, $app->config('debug') );
            $log = "Adding requirement '".$requirement->title."' into req specification '".$requirement->folder->doc_id."': ";
            if( array_key_exists('code', $result) && $result['code']==10000 ){
                $log .= $result['message'];
            } else if( array_key_exists('error_msg', $result) ){
                $log .= "Failed! ".$result['error_msg'];
            } else if( array_key_exists('status_ok', $result) && $result['status_ok']==0 ){
                $log .= "Requirement not created: ".$result['msg'];
            } else if( array_key_exists('msg', $result) && $result['status_ok']==1 ){
                $log .= "OK! ".$result['msg'];
            }
            $logs[] = $log;
        }
        return $logs;
    }
    
    /**
     * Convert file into XML format
     * 
     * @return string
     */
    public function toXML(){
        
    }
}
