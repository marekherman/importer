<?php
class TestUser extends TestEntity{

    /**
     * Find user details (including ID) by username
     * 
     * @global \Slim\Slim $app
     * @param string $username
     * @return array
     */
    public function getUserByLogin($username){
        global $app;
        
        $api = new Api;
        $args = array('user'=>$username);
        $user = $api->query("getUserByLogin", $args, $app->config('debug'));
        /* save user attributes: */
        foreach($user as $key=>$value){
            if( !empty($value) ){
                $this->$key = $value;
            }
        }
        return $user;
    }
}