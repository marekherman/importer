<?php

class GetMethodTest extends Slim_Framework_TestCase {
    
    /* Version test */
    public function testVersion() {
        $this->get('/version');
        $this->assertEquals(200, $this->response->status());
        $this->assertEquals($this->app->config('version'), $this->response->body());
    }
    
    /**
     * System information
     */
    public function testGetApiConnect() {
        $this->get('/api-connect');
        $this->assertEquals(200, $this->response->status());
    }
    public function testGetRoot() {
        $this->get('/');
        $this->assertEquals('200', $this->response->status());
    }
}