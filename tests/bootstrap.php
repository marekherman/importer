<?php
// Settings to make all errors more obvious during testing
error_reporting(-1);
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);

if (!defined('APPLICATION_PATH')) {
    define('APPLICATION_PATH', realpath(dirname(__DIR__)));
}

$_SERVER['REQUEST_METHOD']  = isset($_SERVER['REQUEST_METHOD']) ? $_SERVER['REQUEST_METHOD'] : "GET";
$_SERVER['REMOTE_ADDR']     = isset($_SERVER['REMOTE_ADDR']) ? $_SERVER['REMOTE_ADDR'] : "127.0.0.1";
$_SERVER['REQUEST_URI']     = isset($_SERVER['REQUEST_URI']) ? $_SERVER['REQUEST_URI'] : "127.0.0.1";
$_SERVER['SERVER_NAME']     = isset($_SERVER['SERVER_NAME']) ? $_SERVER['SERVER_NAME'] : "localhost";

date_default_timezone_set('CET');
use There4\Slim\Test\WebTestCase;
define('PROJECT_ROOT', realpath(__DIR__ . '/..'));
require_once PROJECT_ROOT.'/vendor/autoload.php';
require_once 'Slim_Framework_TestCase.php';

spl_autoload_register(function ($class) {
    require_once PROJECT_ROOT.'/classes/'.$class.'.class.php';
});

$app = new \Slim\Slim(array(
    'version'        => '0.0.0',
    'debug'          => false,
    'mode'           => 'testing',
    'templates.path' => __DIR__ . '/../templates'
));

$app->container->singleton('api',  function() { return new Api(); });
$app->container->singleton('form',  function() { return new Form(); });

// Initialize copy of the slim application
class LocalWebTestCase extends WebTestCase
{    
    public function getSlimInstance() {
      global $app;
      // Include our core application file
      require PROJECT_ROOT . '/app/app.php';
      return $app;
    }
};