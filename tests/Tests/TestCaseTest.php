<?php
class TestCaseTest extends \PHPUnit_Framework_TestCase
{
    public function testAddStep()
    {
        $testCase = new TestCase();
        $testCase->addStep('Test title', 9, 2);
        
        $this->assertCount(1, $testCase->steps);
        $this->assertEquals('Test title', $testCase->steps[0]['actions']);
    }
}