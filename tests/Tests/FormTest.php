<?php
class FormTest extends \PHPUnit_Framework_TestCase
{   
    protected $fileName = "";
    
    public function testGetFileType() {
        $form = new Form;
        $form->values['entityType'] = "requirements";
        $form->values['hasHeaders'] = true;
        $form->values['project']    = "123";
        
        $filename = "./public/files/requirements.xlsx";
        $class = $form->getFileType($filename);
        $this->assertInstanceOf("FileTestRequirement", $class);
    }
    
    public function testBadFileExtension() {
        $this->setExpectedException('Exception');
        
        $form = new Form;
        $form->values['entityType'] = "cases";
        $form->values['hasHeaders'] = true;
        $form->values['project']    = "123";
        
        $filename = "test.exe";
        $form->getFileType($filename);
    }
    
    public function testSetProject() {
        $this->setExpectedException('Exception');
        $form = new Form;
        $form->setProject("_?::\\/x;");
    }
    
    public function testSetFileName() {
        $filename = "testCases218.xlsx";
        
        $form = new Form;
        $form->setFileName($filename);
        
        $expected = "testCases218_".time().".xlsx";
        $this->assertEquals($expected, $form->values['fileName']);
    }
}
