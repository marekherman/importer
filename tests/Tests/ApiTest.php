<?php
class ApiTest extends \PHPUnit_Framework_TestCase
{   
    protected $addressInvalid = "http://www.invalidlink.com/xmlrpc.php";
    protected $apiKeyInvalid = "f74da4fb31f65ff373b529c2fea29f5";
    protected $address = "http://37.157.192.236/testlink-1.9.6/lib/api/xmlrpc.php";
    protected $apiKey = "f74da4fb31f65ff373bb269c2fea29f5";
    
    public function testCheckConnectionInvalid() {
        $this->setExpectedException('Exception');
        $api = new Api();
        $api->checkConnection($this->addressInvalid, $this->apiKeyInvalid);
    }
    public function testCheckConnectionValid() {
        $api = new Api();
        $connection = $api->checkConnection($this->address, $this->apiKey);
        $this->assertEquals(true, $connection);
    }
}