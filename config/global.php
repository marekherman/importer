<?php
return array(
    'slim' => array(
        'debug' => true,
        'version' => '1.0',
        'api.supportedVersion' => '1.9.14',
        'createNonexistentSuites'   => true,
        'createNonexistentReqSpecs' => true,
        'overwrite.testresults'     => true,
        'templates.path'            => APPLICATION_PATH.'/templates',
        'cookies.encrypt'           => true,
        'cookies.secret_key' => '717928f6efdb46da9a1578ed463eb4235e2a7f0fb7249c429f5f9d8bac3ee6dc1d44d03fb595daa3280f89fc8de791a477c8e23bd3dbe689055484a74db00cbd',
        'cookies.cipher'            => MCRYPT_RIJNDAEL_256,
        'cookies.cipher_mode'       => MCRYPT_MODE_CBC,
        'sessions.driver' => 'file', // or database
        'sessions.files' => APPLICATION_PATH.'/sessions', // require mkdir
        'temp.files' => APPLICATION_PATH.'/temp',
        'files.size' => ini_get("upload_max_filesize")
    ),
    'twig' => array(
        'environment' => array(
            'charset' => 'utf-8',
            'cache' => realpath(APPLICATION_PATH.'/templates/cache'),
            'auto_reload' => false,
            'strict_variables' => true,
            'autoescape' => true,
            'debug' => false,
        ),
    ),
    'session_cookies' => array(
        'expires' => '2 weeks',
    ),
);
