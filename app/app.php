<?php
// Create monolog logger and store logger in container as singleton
$app->container->singleton('log', function () {
    $log = new \Monolog\Logger('slim-skeleton');
    $log->pushHandler(new \Monolog\Handler\StreamHandler(APPLICATION_PATH.'/logs/app.log', \Monolog\Logger::DEBUG));
    return $log;
});
$app->container->singleton('api',  function() { return new Api(); });
$app->container->singleton('form',  function() { return new Form(); });

// Prepare views
$app->view(new \Slim\Views\Twig($config['twig']));
$app->view->parserExtensions = array(new \Slim\Views\TwigExtension());



/** Home screen */
$app->get('/', '\Validator::checkFolders',  function () use ($app) {$app->render('welcome.php');});

/** First form screen */
$app->get('/api-connect(/)', '\Form:getApiConnect');
$app->post('/api-connect(/)', '\Validator::sanitizeApi', '\Form:postApiConnect');

/** Second form screen */
$app->get('/file-upload(/)', '\Form:getFileUpload');
$app->get('/file-upload-test(/)', '\Form:readUploadedFile');

/** Third form screen */
$app->get('/file-process(/)', '\Validator::sanitizeProcess', '\Form:readUploadedFile');

/** File processing */
$app->post('/file-upload(/)', '\Validator::sanitizeFile', '\Form:postFileUpload');

/** Fourth form screen */
$app->get('/file-import(/)', '\Validator::sanitizeProcess',  function () use ($app) {
    $app->render('import-disallowed.php');
})->name('import');
$app->post('/file-import(/)', '\Form:import');

/* Get version of application */
$app->get('/version(/)', function() use($app) { echo $app->config('version'); });

/* Menu items and templates */
$app->get('/faq(/)', function () use ($app) {$app->render('faq.php');});
$app->get('/file-format(/)', function () use ($app) {$app->render('file-format.php');});
$app->get('/input-requirement(/)', function () use ($app) {$app->render('input-requirement.php');});
$app->get('/input-testcase(/)', function () use ($app) {$app->render('input-testcase.php');});
$app->get('/input-testresult(/)', function () use ($app) {$app->render('input-testresult.php');});
