{% extends 'layout.php' %}

{% block page_title %}Import page{% endblock %}

{% block content %}
<p>You can import a file by following the steps in the menu. You cannot access the import page directly.</p>
{% endblock %}