{% extends 'layout.php' %}

{% block page_title %}Connecting to API{% endblock %}

{% block content %}
<?php
/**
 * http://127.0.0.1/testlink/lib/api/xmlrpc/v1/xmlrpc.php
 * http://37.157.192.236/testlink-1.9.6/lib/api/xmlrpc.php
 */
?>
{% if url!=null and projects != null %}
    <div class="alert alert-success">You already provided your API details. You can go directly to "Upload file" in the menu.
        Alternatively, you can provide different login details below.</div>
{% endif %}
<div class="well">
    <h2>Provide your access details</h2>
    <form role="form" action="" method="post">
      <div class="form-group">
        <label for="url">Your TestLink (>=1.9.14) API address (xmlrpc.php):</label>
        <input type="text" class="form-control" id="url" name="url" placeholder="http://company.com/testlink/lib/api/v1/xmlrpc.php">
      </div>
      <div class="form-group">
        <label for="apikey">API key:</label>
        <input type="text" class="form-control" id="apikey" name="apikey" placeholder="32-character hash found in My Settings -> API interface">
      </div>
      <div class="form-group">
        <label for="author">Username (default Testlink username is admin):</label>
        <input type="text" class="form-control" id="author" name="author" placeholder="If left empty, 'admin' username will be used">
      </div>
      <div class="form-group">
        <div class="col-xs-8 col-sm-10 text-muted">
            <strong>Sample data (local)</strong>:<br />
            Username: admin<br />
            API address: http://127.0.0.1/testlink/lib/api/xmlrpc/v1/xmlrpc.php<br />
            API key: ddf358e364fb3e1ac2cd784bf058f013<br /><br />
            <strong>Sample data (server)</strong>:<br />
            API address: http://39841.w41.wedos.ws/testlink-1.9.14/lib/api/xmlrpc/v1/xmlrpc.php<br />
            API key: a292fff56aa40ef9a43767f7e8669e0b<br /><br />
            <strong>Testlink installation (server)</strong>:<br />
            TestLink server: http://39841.w41.wedos.ws/testlink-1.9.14/ (admin;admin)
        </div>
        <button type="submit" class="btn btn-primary col-xs-4 col-sm-2 pull-right">Continue</button>
      </div>
    </form>
    <div class="clearfix">&nbsp;</div>
</div>
{% endblock %}