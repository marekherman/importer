{% extends 'layout.php' %}

{% block page_title %}Supported file formats{% endblock %}

{% block content %}
<div class="row">
    <div class="col-xs-8">
        <h3>Test requirements</h3>
        <p>Test requirements always belong to a test requirements specification.</p>
        <a href="input-requirement"><img src="./images/testrequirementsstructure.jpg" width="350" alt="Test requirements structure" /></a>
        <p>In this image, there is project "NovyProjekt" with specification "Specifications for ABC" inside of which are two requirements named "Requirement A" and "Requirement B".<br />
            To import a test requirement, you <a href="input-requirement">need to specify its test specification ID</a> (e.g. sub3).</p>
        
        <h3>Test cases</h3>
        <p>Test cases always belong to a test suite and have one or more steps.</p>
        <a href="input-testcase"><img src="./images/testcasestructure.jpg" width="350" alt="Test case structure" /></a>
        <p>In this image, there is project "Projekt2" with "Suite2" inside of which are two test cases named "Case A" and "Case B".<br />
            To import a test case, you <a href="input-testcase">need to specify its test suite name</a> (e.g. Suite2).</p>
        
        <h3>Test plan results</h3>
        <p>Test plan results always belong to a test plan and a test case.</p>
        <a href="input-testresult"><img src="./images/testresultsstructure.jpg" width="350" alt="Test result structure" /></a>
        <p>In this image, there is project "NovyProjekt" with test plan "TestPlan" inside of which are four test results.<br />
            To import a test plan result, you <a href="input-testresult">need to specify test plan name and test case name</a> (e.g. TestPlan and Your first TestCase).</p>
    </div>
    <div class="col-xs-4 well well-sm">
        {% include 'file-description.php' %}
    </div>
</div>
{% endblock %}