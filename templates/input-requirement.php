{% extends 'layout.php' %}

{% block page_title %}Sample requirements input file{% endblock %}

{% block content %}

<div class="container">
    <h2>Example of a requirements Excel file</h2>
    <div class="row">
        <div class="col-xs-5">
            <div class="bs-component">
              <div class="btn-group btn-group-justified">
                <a href="input-requirement" class="btn btn-warning">Requirements</a>
                <a href="input-testcase" class="btn btn-default">Test cases</a>
                <a href="input-testresult" class="btn btn-default">Test plan results</a>
              </div>
            </div>
        </div>
        <div class="col-xs-7">
            <a href="./files/requirements.xlsx" class="btn btn-info">Download sample Excel file</a>
            <a href="./files/create-req-spec.xml" class="btn btn-default">Import specifications (XML) for sample file</a>
        </div>
    </div>
    <p class="text-danger"><strong>Bolded columns are mandatory.</strong>
    <table class="table table-striped table-hover table-condensed">
        <thead>
            <tr>
                <td colspan="1" class="text-center warning">Req spec (column 1)</td>
                <td colspan="8" class="text-center info">Requirement Content (columns 2-9)</td>
            </tr>
        </thead>
        <tbody>
            <tr>
                <td>Req spec ID <a href="#reqspec-id" onClick="$('#folder-name').css('text-decoration','underline')">(help)</a></td>
                <td>Requirement ID</td>
                <td>Requirement title</td>
                <td>Node order</td>
                <td>Requirement Description</td>
                <td>Requirement status <a href="#status" onClick="$('#req-status').css('text-decoration','underline')">(help)</a></td>
                <td>Requirement type <a href="#type" onClick="$('#req-type').css('text-decoration','underline')">(help)</a></td>
                <td>Expected coverage <a href="#expected-coverage" onClick="$('#expected-coverage').css('text-decoration','underline')">(help)</a></td>
                <td>Scope</td>
            </tr>
            <tr>
                <th>sub1</th>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
            </tr>
            <tr>
                <td></td>
                <th>Req-SandBox-1</th>
                <th>The title of the requirement Req-SandBox-1</th>
                <td></td>
                <th>"Description of the Requirement. Could be on multiline, but import will display it without Carriage Return"</th>
                <td>F</td>
                <td>3</td>
                <td>1</td>
                <td></td>
            </tr>
            <tr>
                <td></td>
                <th>Req-SandBox-2</th>
                <th>The title of the requirement Req-SandBox-2</th>
                <td></td>
                <th>"Description of the Requirement. Could be on multiline, but import will display it without Carriage Return"</th>
                <td>F</td>
                <td>3</td>
                <td>1</td>
                <td></td>
            </tr>
            <tr>
                <th>sub2</th>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
            </tr>
            <tr>
                <td></td>
                <th>Req-SandBox-3</th>
                <th>The title of the requirement Req-SandBox-3</th>
                <td></td>
                <th>"Description of the Requirement. Could be on multiline, but import will display it without Carriage Return"</th>
                <td>F</td>
                <td>3</td>
                <td>1</td>
                <td></td>
            </tr>
            <tr>
                <td></td>
                <th>Req-SandBox-4</th>
                <th>The title of the requirement Req-SandBox-4</th>
                <td></td>
                <th>"Description of the Requirement. Could be on multiline, but import will display it without Carriage Return"</th>
                <td>F</td>
                <td>3</td>
                <td>2</td>
                <td></td>
            </tr>
        </tbody>
    </table>
    <p class="text-danger"><strong>Bolded columns are mandatory.</strong>
</div>
<hr />

<a name="reqspec-id" id="folder-name">Requirement specification ID</a>
    <li><ul>Case sensitive</ul></li>
    <li><ul>When in Navigator - Requirement Specifications of your TestLink, you will see for example <a href="./file-format"><code>sub3:Specifications for ABC</code></a> in the tree.
            <code>sub3</code> is the ID which you need to provide. You can choose any naming of the IDs you wish in your project.</ul></li>
    <li><ul>Requirements are imported into the req. specification above them. E.g. "Req-SandBox-1" and "Req-SandBox-2" will be imported into requirement specification (folder) "sub1".</ul></li>
<a name="status" id="req-status">Requirement status</a>
    <li><ul>D (Draft), R (Review), W (Rework), F (Finish), I (Implemented), V (Valid), N (Not Testable), O (Obsolete)</ul></li>
<a name="type" id="req-type">Requirement type</a>
    <li><ul>1 (Informational), 2 (Feature), 3 (Use Case), 4 (User Interface), 5 (Non-functional), 6 (Constraint), 7 (System Function)</ul></li>
<a name="expected-coverage" id="expected-coverage">Expected coverage</a>
    <li><ul>Number of tests (in testcases) that must be attached to the
requirement to cover the requirement. If not filled, automatically set to 0 by testlink
during import. 0 is a valid value during import but is invalid if requirement is edited in
TestLink GUI later.</ul></li>

    
{% endblock %}