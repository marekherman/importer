{% extends 'layout.php' %}

{% block page_title %}Confirm the uploaded content{% endblock %}

{% block content %}

<h3>Import finished</h3>
<p>Your file was imported. Result of the import can be found below:</p>

<div class="well well-sm">
    {% for key,logrow in log %}
        {% if 'ok' in logrow %}
            <code class='success'>
        {% else %}
            <code>
        {% endif %}
            {{loop.index}}&nbsp;>&nbsp;{{ logrow }}<br />
            </code>
    {% endfor %}
</div>

<p>More information can be found by setting debug to true in config/global.php.</p>

<a href='/file-upload' class='btn btn-success col-xs-8 col-xs-offset-2'>
    <i class="glyphicon glyphicon-upload"></i>&nbsp;Upload another file</a>
<div class='clearfix'>&nbsp;</div>

{% endblock %}