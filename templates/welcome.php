{% extends 'layout.php' %}

{% block page_title %}Welcome to Testlink importer{% endblock %}

{% block content %}
<div class="well">
    <h3>Welcome to TestLink import tool.</h3>
    <p>You are able to import XLS, XLSX, CSV and XML files into your TestLink.</p>
    <p>You can import test requirements, test cases (in suites) and test case results.</p>
    <p>At the next screen you will be asked to provide your <strong>TestLink installation URL</strong> and your <strong>API key</strong>.</p>
    <a href="./api-connect" class="btn btn-primary col-xs-4 col-xs-offset-4">Continue</a>
    <div class="clearfix">&nbsp;</div>
</div>
{% endblock %}