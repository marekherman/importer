{% extends 'layout.php' %}

{% block page_title %}Result of test requirements import{% endblock %}

{% block content %}


<form action="file-import" method="post" role="form">
<div class="well well-sm">
<table class="table table-condensed table-striped">
    <tr>
        <th>ID</th>
        <th>Title</th>
        <th>Folder name</th>
        <th>Node order</th>
        <th>Description</th>
        <th>Status</th>
        <th>Type</th>
        <th>Expected coverage</th>
        <th>Scope</th>
    </tr>
{% for key,row in data %}
    {% if loop.first %}
        <h3>Importing test requirements into <strong>{{project.name|striptags|escape("html")}}</strong></h3>
        <span class="text-muted">Data hasn't been imported yet, confirm to do the import</span>
    {% endif %}
    <tr>
        <td><label><input type="checkbox" name="confirmed[]" value="{{row.title|striptags|escape("html")}}" checked="checked" />&nbsp;
                {{row.id|striptags|escape("html")}}</label></td>
        <td>{{row.title|striptags|escape("html")}}</td>
        <td>{{row.folder.doc_id|striptags|escape("html")}}</td>
        <td>{{row.node_order|striptags|escape("html")}}</td>
        <td>{{row.description|striptags|escape("html")}}</td>
        <td>{{row.status|striptags|escape("html")}} {% if row.status is empty %}<span class="text-danger">(empty)*</span>{% endif %}</td>
        <td>{{row.type|striptags|escape("html")}}</td>
        <td>{{row.expectedCoverage|striptags|escape("html")}}</td>
        <td>{{row.scope|striptags|escape("html")}}</td>
            
        
    </tr>
{% endfor %}
</table>
    <ul>
        <li>D (Draft) status will be used for <span class="text-danger">(empty)*</span> values.</li>
    </ul>
    <button type="submit" class="btn btn-primary"><i class="glyphicon glyphicon-ok"></i>&nbsp;Everything is OK, do the import!</button>
</div>
</form>

{% endblock %}