{% extends 'layout.php' %}

{% block page_title %}Connecting to API{% endblock %}

{% block content %}

<div class='row'>
    
    {% if url == null %}
        <div class="alert alert-danger">You did not connect to a TestLink installation. Please go to the previous step and do it first.</div>
    {% endif %}
    {% if url!=null and projects == null %}
        <div class="alert alert-danger">Your TestLink installation does not contain any projects. Add some and connect again after that.</div>
    {% endif %}
    
    <div class='col-xs-6 col-sm-8 well well-sm'>
        <form method='post' action='' enctype="multipart/form-data">
            <fieldset>
                <div class="form-group">
                    <label class="col-xs-12 col-sm-4 control-label">Content of uploaded file</label>
                    <div class="col-xs-12 col-sm-8">
                      <div class="radio">
                        <label>
                          <input type="radio" name="entityType" id="optionsRadios1" value="requirements">
                          Test requirements (<a href="./input-requirement" target='_blank'>example</a>)
                        </label>
                      </div>
                      <div class="radio">
                        <label>
                          <input type="radio" name="entityType" id="optionsRadios2" value="tests">
                          Test cases (<a href="./input-testcase" target='_blank'>example</a>)
                        </label>
                      </div>
                      <div class="radio">
                        <label>
                          <input type="radio" name="entityType" id="optionsRadios3" value="testsresults">
                          Tests with results (<a href="./input-testresult" target='_blank'>example</a>)
                        </label>
                      </div>
                    </div>
                    <hr />
                </div>
                <div class='form-group'>
                    <div class='row'>
                        <div class="col-xs-12 col-sm-4 text-center"><label class="control-label">Project to upload into:</label></div>
                        <div class="col-xs-12 col-sm-8">
                            <select class="form-control" name="projectId" id="projectId">
                                {% for project in projects %}
                                        <option value="{{project.id}}">{{project.name|striptags|escape("html")}} (ID: {{project.id}})</option>
                                {% endfor %}
                            </select>
                        </div>
                    </div>
                    <hr />
                </div>
                <div class='form-group'>
                    <div class='row'>
                        <div class="col-xs-12 col-sm-4 text-center"><label class="control-label">Ignore first row:</label></div>
                        <div class="col-xs-12 col-sm-8"><input type="checkbox" name="hasHeaders" value='true' checked='checked' id="hasHeaders" />
                            File has headers (ignore first row)</div>
                    </div>
                </div>
                <div class='form-group'>
                    <div class='row'>
                        <div class="col-xs-12 col-sm-4 text-center"><label class="control-label">Upload file:</label></div>
                        <div class="col-xs-12 col-sm-8"><input type="file" name="file" /></div>
                    </div>
                    <hr />
                    <p>This server allows upload size: {{ filesize }}</p>
                    <p>Your TestLink installation most likely supports 400 Kb upload size. You can change this in your custom_cfg.inc.php file.</p>
                    <hr />
                </div>
                <div class='form-group'>
                    {% if fileName is not null  %}
                    <p class="text-center">You already uploaded a file <strong>({{ fileName }})</strong> earlier.</p>
                    <p class="text-center text-danger"><i class="glyphicon glyphicon-arrow-left"></i>To use the old file, click the red button.</p>
                    <p class="text-center text-success">To use a new file and/or new project, fill out the form above and
                        click the green button.<i class="glyphicon glyphicon-arrow-right"></i></p>
                    <a href="file-process" class="btn btn-primary pull-left col-xs-3">Continue with existing file</a>
                    {% endif %}
                    <button type="submit" class="btn btn-success pull-right col-xs-3">
                        Submit {% if fileName is not null  %}a new file{% endif %}
                    </button>
                </div>
            </fieldset>
        </form>
    </div>
    <div class='col-xs-6 col-sm-4'>
        {% include 'file-description.php' %}
    </div>
</div>

{% endblock %}