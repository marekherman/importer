{% extends 'layout.php' %}

{% block page_title %}Result of test results import{% endblock %}

{% block content %}


<form action="file-import" method="post" role="form">
<div class="well well-sm">
<table class="table table-condensed table-striped">
    <tr>
        <th>Test case name</th>
        <th>Test plan name</th>
        <th>Build ID</th>
        <th>Status (result)</th>
        <th>Notes</th>
        <th>Tester</th>
        <th>Timestamp</th>
        <th>Bug ID</th>
    </tr>
{% for key,row in data %}
    {% if loop.first %}
        <h3>Importing test results into <strong>{{project.name|striptags|escape("html")}}</strong></h3>
        <span class="text-muted">Data hasn't been imported yet, confirm to do the import</span>
    {% endif %}
    <tr>
        <td><label><input type="checkbox" name="confirmed[]" value="{{row.testName|striptags|escape("html")}}" checked="checked" />&nbsp;
                {{row.testName|striptags|escape("html")}}</label></td>
        <td>{{row.testPlanName|striptags|escape("html")}}</td>
        <td>{{row.buildId|striptags|escape("html")}} {% if row.buildId is empty %}<span class="text-danger">(empty)*</span>{% endif %}</td>
        <td>{{row.status|striptags|escape("html")}}</td>
        <td>{{row.notes|striptags|escape("html")}}</td>
        <td><span class="{% if row.tester=='admin' %}text-success{% endif %}">{{row.tester|striptags|escape("html")}}</span>
            {% if row.tester is empty %}<span class="text-success">(empty)*</span>{% endif %}</td>
        <td>{{row.timestamp|striptags|escape("html")}}</td>
        <td>{{row.bugId|striptags|escape("html")}}</td>
            
        
    </tr>
{% endfor %}
</table>
    <ul>
        <li>Checked test results will be imported. Unchecked test results will be skipped.</li>
        <li>Existing test results will be overwritten.</li>
        <li>Latest build of test case will be used for <span class="text-danger">(empty)*</span> values.</li>
        <li>User 'admin' will be used for <span class="text-success">(empty)*</span> values.</li>
    </ul>
    <button type="submit" class="btn btn-primary"><i class="glyphicon glyphicon-ok"></i>&nbsp;Everything is OK, do the import!</button>
</div>
</form>

{% endblock %}