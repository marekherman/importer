{% extends 'layout.php' %}

{% block page_title %}Connecting to API{% endblock %}

{% block content %}
<h2>Installation</h2>
<p>If you want to install this tool on your server, please make sure that the following criteria are met:</p>
<ul>
    <li>sessions folder must be writable</li>
    <li>temp folder must be writable</li>
    <li>templates folder must be readable</li>
    <li>PHP >=5.3.0 must be installed</li>
</ul>
<hr />
<h3>Importing files</h3>
<p>Import will work with TestLink 1.9.14 and higher. If you use older version, please upgrade TestLink to 1.9.14 or higher.</p>
<p>Unfortunately, TestLink API doesn't support the necessary things so you need to do a few changes. You have two options:</p>
<ol>
    <li>Copy <a href="/lib/api/xmlrpc/v1/xmlrpc.class.phps" target="_blank" style="text-decoration:underline"><code>xmlrpc.class.php(s)</code></a> and 
        <a href="/lib/functions/requirement_spec_mgr.class.php" target="_blank" style="text-decoration:underline"><code>requirement_spec_mgr.class.php</code></a>
        files from <code>/lib/*</code> folders of this application into your TestLink</li>
    <li>Do the necessary changes manually (see below)</li>
</ol>
<ul>
    <li><strong>Option #1</strong> is easier but if you want to copy these files into version other (higher) than 1.9.14, you must merge these files and make sure you do not delete any changes made in the new version.</li>
    <li><strong>Option #2</strong> is more complicated but will work if Option #2 cannot be used.</li>
</ul>

<p><em>It is possible that the necessary changes will be made in version 1.9.16 or further. In that case, this will no longer be necessary.</em></p>

<hr />
<h3>Manual changes (Option #2)</h3>
Do this if you want to make the changes manually.
<h4>Importing requirements</h4>
<p>Unfortunately, importing requirements is not supported by TestLink API by default. So you need to do a few changes:</p>
<ul>
    <li>copy the <code>createRequirement</code> method from latest version on <a href="http://mantis.testlink.org/view.php?id=4940">http://mantis.testlink.org/view.php?id=4940</a> to <strong>xmlrpc.class.php</strong> file of your TestLink</li>
    <li>add <code>'tl.createRequirement' => 'this:createRequirement',</code> to <code>$this->methods</code> in <code>initMethodYellowPages()</code> method in <strong>xmlrpc.class.php</strong> file</li>
    <li>Add <code>getRequirementSpecifications</code> method from below to <strong>xmlrpc.class.php</strong> file of your TestLink
        <pre><code>    public function getRequirementSpecifications($args)
	{       
	    $result=array();
	    $this->_setArgs($args);
	    $operation=__FUNCTION__;
            $msg_prefix="({$operation}) - ";
            
            $tproject_id=$this->args[self::$testProjectIDParamName];
            $ret = $this->reqSpecMgr->get_all_in_testproject($tproject_id);
            return $ret;
            }</code></pre></li>
    <li>add <code>'tl.getRequirementSpecifications' => 'this:getRequirementSpecifications',</code> to <code>$this->methods</code> in <code>initMethodYellowPages()</code> method in <strong>xmlrpc.class.php</strong> file</li>
    <li>In <code>lib/functions/requirement_spec_mgr.class.php</code> file of your TestLink find method <code>get_all_in_testproject</code> and replace the existing query as shown below (original query causes errors):
        <pre><code>
function get_all_in_testproject($tproject_id,$order_by=" ORDER BY title")
{                               
   	$debugMsg = 'Class:' . __CLASS__ . ' - Method: ' . __FUNCTION__;
// 	$sql = "/* $debugMsg */ " . 
// 	       " SELECT RSPEC.id,testproject_id,RSPEC.scope,RSPEC.total_req,RSPEC.type," .
//            " RSPEC.author_id,RSPEC.creation_ts,RSPEC.modifier_id," .
//            " RSPEC.modification_ts,NH.name AS title,NH.node_order " .
// 	       " FROM {$this->object_table} RSPEC, {$this->tables['nodes_hierarchy']} NH " .
// 	       " WHERE NH.id=RSPEC.id" .
// 	       " AND testproject_id={$tproject_id}";
	$sql = "/* $debugMsg */ " . 
	       " SELECT RSPEC.id,testproject_id,doc_id," .
           " NH.name AS title,NH.node_order, NH.parent_id, NH.node_type_id, NH.node_order " .
	       " FROM {$this->object_table} RSPEC, {$this->tables['nodes_hierarchy']} NH " .
	       " WHERE NH.id=RSPEC.id" .
	       " AND testproject_id={$tproject_id}";
        </code></pre>
        
</ul>

<h4>Importing test cases</h4>
<p>Unfortunately, importing test cases is not well supported by TestLink API by default. So you need to do a few changes:</p>
<ul>
    <li>copy the <code>getTestSuite</code> method from latest version on <a href="http://mantis.testlink.org/view.php?id=7326">http://mantis.testlink.org/view.php?id=7326</a> to <strong>xmlrpc.class.php</strong> file of your TestLink</li>
    <li>add <code>'tl.getTestSuite' => 'this:getTestSuite',</code> to <code>$this->methods</code> in <code>initMethodYellowPages()</code> method in <strong>xmlrpc.class.php</strong> file</li>
</ul>

{% endblock %}