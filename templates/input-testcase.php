{% extends 'layout.php' %}

{% block page_title %}Sample test case input file{% endblock %}

{% block content %}

<div class="container">
    <h2>Example of a test case Excel file</h2>
    <div class="row">
        <div class="col-xs-5">
            <div class="bs-component">
              <div class="btn-group btn-group-justified">
                <a href="input-requirement" class="btn btn-default">Requirements</a>
                <a href="input-testcase" class="btn btn-warning">Test cases</a>
                <a href="input-testresult" class="btn btn-default">Test plan results</a>
              </div>
            </div>
        </div>
        <div class="col-xs-7">
            <a href="./files/testcases.xlsx" class="btn btn-info">Download sample Excel file</a>
        </div>
    </div>
    <p class="text-danger"><strong>Bolded columns are mandatory.</strong>
    <table class="table table-striped table-hover table-condensed">
        <thead>
            <tr>
                <td colspan="2" class="text-center warning">Test suite (col 1-2)</td>
                <td colspan="8" class="text-center info">Test case (col 3-10)</td>
                <td colspan="3" class="text-center warning">Steps (col 11-13)</td>
                <td colspan="2" class="text-center info">Coverage (col 14-15)</td>
            </tr>
        </thead>
        <tbody>
            <tr>
                <td>Test suite name <a href="#suite-name" onClick="$('#suite-name').css('text-decoration','underline')">(help)</a></td>
                <td>Test suite details</td>
                <td>Test case (TC) number</td>
                <td>TC name <a href="#tc-name" onClick="$('#tc-name').css('text-decoration','underline')">(help)</a></td>
                <td>TC summary</td>
                <td>TC precondition</td>
                <td>TC execution type <a href="#execution-type" onClick="$('#execution-type').css('text-decoration','underline')">(help)</a></td>
                <td>TC importance <a href="#importance" onClick="$('#importance').css('text-decoration','underline')">(help)</a></td>
                <td>Custom column #1</td>
                <td>Custom column #2</td>
                <td>Step</td>
                <td>Expected result</td>
                <td>Step execution type <a href="#step-execution-type" onClick="$('#step-execution-type').css('text-decoration','underline')">(help)</a></td>
                <td>Requirement title</td>
                <td>Requirement document ID <a href="#document-id" onClick="$('#document-id').css('text-decoration','underline')">(help)</a></td>
            </tr>
            <tr>
                <th>myTestSuite</th>
                <td>Details about my TestSuite</td>
                <td colspan="13"></td>
            </tr>
            <tr>
                <td></td>
                <td></td>
                <td></td>
                <th>A first TestCase</th>
                <td>The summary of the First TestCase</td>
                <td></td>
                <td></td>
                <th>1</th>
                <td></td>
                <td></td>
                <th>The first Step</th>
                <th>Expected result for the first step</th>
                <td></td>
                <td></td>
                <td>Req-SandBox-1</td>
            </tr>
            <tr>
                <td></td>
                <td></td>
                <td></td>
                <th>A second TestCase</th>
                <td>A second test Case for My TestSuite</td>
                <td>A precondition is defined</td>
                <td></td>
                <th>2</th>
                <td></td>
                <td></td>
                <th>The first Step</th>
                <th>Expected result for the first step</th>
                <td></td>
                <td></td>
                <td>Req-SandBox-2</td>
            </tr>
            <tr>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td>A second Step</td>
                <td>And an expected result for the second step</td>
                <td></td>
                <td></td>
                <td></td>
            </tr>
            <tr>
                <th>YourTestSuite</th>
                <td>Details about your TestSuite</td>
                <td colspan="13"></td>
            </tr>
            <tr>
                <td></td>
                <td></td>
                <td></td>
                <th>Your first TestCase</th>
                <td>The summary of the First TestCase of your testSuite</td>
                <td>Your precondition</td>
                <td></td>
                <th>3</th>
                <td></td>
                <td></td>
                <th>The first Step</th>
                <th>Expected result for the first step</th>
                <td></td>
                <td></td>
                <td>Req-SandBox-3</td>
            </tr>
            <tr>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td>A second Step</td>
                <td>And an expected result for the second step</td>
                <td></td>
                <td></td>
                <td></td>
            </tr>
            <tr>
                <td></td>
                <td></td>
                <td></td>
                <td>Your second TestCase</td>
                <td>A second test Case for Your TestSuite</td>
                <td></td>
                <td>2</td>
                <td></td>
                <td></td>
                <td></td>
                <td>The first Step</td>
                <td>Expected result for the first step</td>
                <td></td>
                <td></td>
                <td>Req-SandBox-2;Req-SandBox-4</td>
            </tr>
            <tr>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td>A second Step</td>
                <td>And an expected result for the second step</td>
                <td></td>
                <td></td>
                <td></td>
            </tr>
            <tr>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td>A Third Step</td>
                <td>Expected result for the third step</td>
                <td></td>
                <td></td>
                <td></td>
            </tr>
            <tr>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
            </tr>
        </tbody>
    </table>
    <p class="text-danger"><strong>Bolded columns are mandatory.</strong>
</div>
<hr />

<a name="suite-name" id="execution-type">Test Suite name</a>
    <li><ul>If you specify a test suite that doesn't exist, it will be automatically created (can be turned off in config/global.php).</ul></li>
<a name="tc-name" id="execution-type">Test case name</a>
    <li><ul>Maximum 100 characters (more will be cropped)</ul></li>
<a name="execution-type" id="execution-type">Test case execution type</a>
    <li><ul>1 (Manual), 2 (Automatic)</ul></li>
<a name="importance" id="importance">Test case importance</a>
    <li><ul>1 (Low), 2 (Medium), 3 (High)</ul></li>
<a name="step-execution-type" id="step-execution-type">Step execution type</a>
    <li><ul>1 (Manual), 2 (Automatic). <strong>If not filled, set to 1 (Manual)</strong>.</ul></li>
<a name="document-id" id="document-id">Document ID</a>
    <li><ul>Document IDs covered by this test case. Multiple document IDs can be separated by semi-comma (;)</ul></li>

{% endblock %}