{% extends 'layout.php' %}

{% block page_title %}Result of test cases import{% endblock %}

{% block content %}


<form action="file-import" method="post" role="form">
<div class="well well-sm">
<table class="table table-condensed table-striped">
    <tr>
        <th>Test case name</th>
        <th>TC #</th>
        <th>Summary</th>
        <th>Test suite</th>
        <th>Importance</th>
        <th>Number of steps</th>
    </tr>
{% for key,row in data %}
    {% if loop.first %}
        <h3>Importing test cases into <strong>{{row.testSuite.project.name|striptags|escape("html")}}</strong></h3>
        <span class="text-muted">Data hasn't been imported yet, confirm to do the import</span>
    {% endif %}
    <tr>
        <td><label><input type="checkbox" name="confirmed[]" value="{{row.name|striptags|escape("html")}}" checked="checked" />&nbsp;{{key|striptags|escape("html")}}</label></td>
        <td>{{row.testCaseNumber|striptags|escape("html")}}</td>
        <td>{{row.summary|striptags|escape("html")}}</td>
        <td>{% if row.testSuite.created==1 %} <span class='text-success'> {% endif %}
            {{row.testSuite.name|striptags|escape("html")}}
            {% if row.testSuite.created==1 %}*</span> {% endif %}
        </td>
        <td>{{row.importance|striptags|escape("html")}}</td>
        <td>{{row.steps|length }}</span></td>
            
        
    </tr>
{% endfor %}
</table>
    <ul>
        <li>Checked test cases will be imported. Unchecked test cases will be skipped.</li>
        <li><strong class='text-success'>* Test suites marked green</strong> were automatically created because they didn't exist.</li>
    </ul>
    <button type="submit" class="btn btn-primary"><i class="glyphicon glyphicon-ok"></i>&nbsp;Everything is OK, do the import!</button>
</div>
</form>

{% endblock %}