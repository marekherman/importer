<h3>Requirements</h3>
<p>Requirements describe the intended purpose, what the software will do and how it is expected to perform.</p>
<p>In TestLink, they belong to a requirements specification and they have ID, title, description and more optional parameters.<br />
<ul>
    <li><a href="./input-requirement">Sample requirement format FAQ</a></li>
    <li><a href="./files/requirements.xlsx">Excel file sample</a></li>
</ul>
</p>

<h3>Test cases</h3>
<p>Tests cases are the basic parts of TestLink. They are documents that describe what needs to be tested and contain a known input, conditions and expected output.</p>
<p>In TestLink, test cases have title, summary, steps, expected results  and more optional parameters.</p>
<ul>
    <li><a href="./input-testcase">Sample test case format FAQ</a></li>
    <li><a href="./files/testcases.xlsx">Excel file sample</a></li>
</ul>

<h3>Test plan results</h3>
<p>Tests plan result contains a report about the outcome of a test case. A test plan result belongs into a test plan inside a project.</p>
<p>In TestLink, they have a result (passed, failed, blocked) and more optional parameters.</p>
<ul>
    <li><a href="./input-testresult">Sample test results format FAQ</a></li>
    <li><a href="./files/testresults.xlsx">Excel file sample</a></li>
</ul>