<!DOCTYPE html>
<html>
    <head>
        <title>{% block page_title %}{{ profile.site_name }}{% endblock %}</title>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js"></script>
        
        <link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css" rel="stylesheet">
        <link href="https://maxcdn.bootstrapcdn.com/bootswatch/3.3.5/united/bootstrap.min.css" rel="stylesheet">
        <link href="./css/custom.css" rel="stylesheet">
    </head>
    <body>
        <div class='navbar navbar-default'>
            <div class='container'>
            
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#collapsableMenu">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                </div>
                
                <div class='navbar-collapse collapse' id='collapsableMenu'>
                    {% set splittedUrl = currentUrl()|split('/') %}
                    {% set page = splittedUrl[splittedUrl|length-1] %}
                    {% if page is empty %}{% set page = splittedUrl[splittedUrl|length-2] %}{% endif %}

                    <ul class="nav navbar-nav">
                        <li {% if page=="index.php" %} class="active" {% endif %}>
                            <a href="./index.php">Welcome&nbsp;<i class='glyphicon glyphicon-chevron-right'></i></a>
                        </li>
                        <li{% if page=="api-connect" %} class="active" {% endif %}>
                            <a href="./api-connect">Connect&nbsp;<i class='glyphicon glyphicon-chevron-right'></i></a>
                        </li>
                        <li {% if page=="file-upload" %} class="active" {% endif %}>
                            <a href="./file-upload">Upload file&nbsp;<i class='glyphicon glyphicon-chevron-right'></i></a>
                        </li>
                        <li {% if page=="file-process" %} class="active" {% endif %}>
                            <a href="./file-process">Confirm&nbsp;<i class='glyphicon glyphicon-chevron-right'></i></a>
                        </li>
                        <li {% if page=="file-import" %} class="active" {% endif %}>
                            <a href="./file-import">Import&nbsp;<i class='glyphicon glyphicon-chevron-right'></i></a>
                        </li>
                    </ul>
                    <ul class="nav navbar-nav navbar-right">
                        <li class="hidden-sm hidden-md {% if page=="file-format" %} active {% endif %}">
                            <a href="./file-format"><i class='glyphicon glyphicon-file'></i>&nbsp;File formats</a>
                        </li>
                        <li {% if page=="faq" %} class="active" {% endif %}>
                            <a href="./faq"><i class='glyphicon glyphicon-th-list'></i>&nbsp;FAQ</a>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
        <div class='container'>
            <div class='page-header'>
                {% if flash.error is defined  %}<div class="row errors alert alert-danger text-center">{{ flash['error'] }}</div>{% endif %}
                {% if flash.notification is defined  %}<div class="row notifications alert alert-info text-center">{{ flash['notification'] }}</div>{% endif %}

                {% block content %} {% endblock %}
            </div>
        </div>
        <footer class="container">
            <p class="text-center">TestLink Import Tool | 2016</p>
            {% block footer %} {% endblock %}
        </footer>
    </body>
</html>
