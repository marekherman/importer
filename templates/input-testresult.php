{% extends 'layout.php' %}

{% block page_title %}Sample test result input file{% endblock %}

{% block content %}

<div class="container">
    <h2>Example of a test results Excel file</h2>
    
    <div class="row">
        <div class="col-xs-5">
            <div class="bs-component">
              <div class="btn-group btn-group-justified">
                <a href="input-requirement" class="btn btn-default">Requirements</a>
                <a href="input-testcase" class="btn btn-default">Test cases</a>
                <a href="input-testresult" class="btn btn-warning">Test plan results</a>
              </div>
            </div>
        </div>
        <div class="col-xs-7">
            <a href="./files/testresults.xlsx" class="btn btn-info">Download sample Excel file</a>
        </div>
    </div>
    <p class="text-danger"><strong>Bolded columns are mandatory.</strong>
    <table class="table table-striped table-hover table-condensed">
        <thead>
            <tr>
                <td colspan="2" class="text-center warning">Test plan and test case (columns 1-2)</td>
                <td colspan="6" class="text-center info">Test result (columns 3-8)</td>
            </tr>
        </thead>
        <tbody>
            <tr>
                <td>Test plan name</td>
                <td>Test case name</td>
                <td>Build ID <a href="#buildid" onClick="$('#buildid').css('text-decoration','underline')">(help)</a></td>
                <td>Result <a href="#result" onClick="$('#result').css('text-decoration','underline')">(help)</a></td>
                <td>Notes</td>
                <td>Tester <a href="#tester" onClick="$('#tester').css('text-decoration','underline')">(help)</a></td>
                <td>Timestamp</td>
                <td>Related bug ID <a href="#bugid" onClick="$('#bugid').css('text-decoration','underline')">(help)</a></td>
            </tr>
            <tr>
                <th>Plan1</th>
                <th>Register with invalid data</th>
                <td></td>
                <th>p</th>
                <td>Notes about result</td>
                <td>admin</td>
                <td>2016-01-10 14:00:00</td>
                <td></td>
            </tr>
            <tr>
                <th>My Plan</th>
                <th>Register with missing data</th>
                <td></td>
                <th>f</th>
                <td>More notes</td>
                <td></td>
                <td>2016-01-10 14:30:00</td>
                <td>24;91;48</td>
            </tr>
            <tr>
                <th>My Plan</th>
                <th>Register with valid data</th>
                <td>3</td>
                <th>p</th>
                <td></td>
                <td>admin</td>
                <td>2016-01-10 14:45:00</td>
                <td></td>
            </tr>
        </tbody>
        
    </table>
    <p class="text-danger"><strong>Bolded columns are mandatory.</strong>
    
</div>
<hr />

<a name="result" id="result">Build ID</a>
    <li><ul>Build ID to which to import test results. If no ID is provided, latest build will be used.</ul></li>
<a name="result" id="result">Result</a>
    <li><ul>p (passed), f (failed), b (blocked)</ul></li>
<a name="tester" id="tester">Tester</a>
    <li><ul>Must be an existing user in your TestLink. If left empty, user whose API was used to connect will be set.</ul></li>
<a name="bugid" id="tester">Bug ID</a>
    <li><ul>Bug ID can be left empty. Multiple IDs can be provided when separated by a semicolon.</ul></li>
    
{% endblock %}