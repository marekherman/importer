# TestLink Import Tool

This application uses the latest Slim and Slim-Views repositories. It also uses Sensio Labs' [Twig](http://twig.sensiolabs.org) template library.
This skeleton application was built for Composer. This makes setting up a new Slim Framework application quick and easy.

## Requirements

If you want to install this tool on your server, please make sure that the following criteria are met:
* sessions folder must be writable
* temp folder must be writable
* templates folder must be readable
* PHP >=5.3.0 must be installed

## Importing files

Import will work with TestLink 1.9.14 and higher. If you use older version, please upgrade TestLink to 1.9.14 or higher.
Unfortunately, TestLink API doesn't support everything to be imported and searched so you need to do a few changes. You have two options:

1) Copy xmlrpc.class.php(s) and requirement_spec_mgr.class.php files from [/lib](/lib) folder of this application into your TestLink
2) Do the necessary changes manually

It is possible that the necessary changes will be made in version 1.9.16 or further. In that case, this will no longer be necessary.

## Manual changes (Option 2)

### Importing requirements
Unfortunately, importing requirements is not supported by TestLink API by default. So you need to do a few changes:

* copy the `createRequirement` method from latest version on [http://mantis.testlink.org/view.php?id=4940](http://mantis.testlink.org/view.php?id=4940) to `xmlrpc.class.php` file of your TestLink
* add `'tl.createRequirement' => 'this:createRequirement',` to `$this->methods` in `initMethodYellowPages()` method in <strong>xmlrpc.class.php</strong> file
* Add `getRequirementSpecifications` method from below to <strong>xmlrpc.class.php</strong> file of your TestLink

```
    public function getRequirementSpecifications($args)
	{       
	    $result=array();
	    $this->_setArgs($args);
	    $operation=__FUNCTION__;
            $msg_prefix="({$operation}) - ";
            
            $tproject_id=$this->args[self::$testProjectIDParamName];
            $ret = $this->reqSpecMgr->get_all_in_testproject($tproject_id);
            return $ret;
        }
```

* add `'tl.getRequirementSpecifications' => 'this:getRequirementSpecifications',` to `$this->methods` in `initMethodYellowPages()` method in `xmlrpc.class.php` file
* In `lib/functions/requirement_spec_mgr.class.php` file of your TestLink find method `get_all_in_testproject` and replace the existing query as shown below (original query causes errors):

```
function get_all_in_testproject($tproject_id,$order_by=" ORDER BY title")
{                               
   	$debugMsg = 'Class:' . __CLASS__ . ' - Method: ' . __FUNCTION__;
// 	$sql = "/* $debugMsg */ " . 
// 	       " SELECT RSPEC.id,testproject_id,RSPEC.scope,RSPEC.total_req,RSPEC.type," .
//            " RSPEC.author_id,RSPEC.creation_ts,RSPEC.modifier_id," .
//            " RSPEC.modification_ts,NH.name AS title,NH.node_order " .
// 	       " FROM {$this->object_table} RSPEC, {$this->tables['nodes_hierarchy']} NH " .
// 	       " WHERE NH.id=RSPEC.id" .
// 	       " AND testproject_id={$tproject_id}";
	$sql = "/* $debugMsg */ " . 
	       " SELECT RSPEC.id,testproject_id,doc_id," .
           " NH.name AS title,NH.node_order, NH.parent_id, NH.node_type_id, NH.node_order " .
	       " FROM {$this->object_table} RSPEC, {$this->tables['nodes_hierarchy']} NH " .
	       " WHERE NH.id=RSPEC.id" .
	       " AND testproject_id={$tproject_id}";
```

### Importing test cases

Unfortunately, importing test cases is not well supported by TestLink API by default. So you need to do a few changes:

* copy the `getTestSuite` method from latest version on [http://mantis.testlink.org/view.php?id=7326](http://mantis.testlink.org/view.php?id=7326) to `xmlrpc.class.php` file of your TestLink
* add `'tl.getTestSuite' => 'this:getTestSuite',` to `$this->methods` in `initMethodYellowPages()` method in `xmlrpc.class.php` file